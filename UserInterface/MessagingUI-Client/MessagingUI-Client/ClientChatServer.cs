﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessagingUI_Client
{
    public partial class ClientChatServer : Form
    {
        public ClientChatServer()
        {
            InitializeComponent();
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "trigger.txt";
            watcher.Changed += new FileSystemEventHandler(this.OnMessageReceived);
            watcher.EnableRaisingEvents = true;
        }
        delegate void InvokeMessageDelegate(string clientName, string message);

        void OnMessageReceived(object source, FileSystemEventArgs e)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                string[] lines = File.ReadAllLines(path);
                if (lines.Length > 1)
                {
                    this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), lines[0], lines[1]);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }

        void InvokeMessage(string IsClientMessage, string message)
        {
            /*TabPage messagePage = null;
            TabControl.TabPageCollection pages = this.messageTabControl.TabPages;
            foreach (TabPage page in pages)
            {
                if (page.Name.Equals(clientName + "tabPage"))
                {
                    messagePage = page;
                    break;
                }
            }*/

            /*if (messagePage == null)
            {
                messagePage = new TabPage();
                messagePage.Name = clientName + "tabPage";
                messagePage.Text = clientName;
                messagePage.Height = this.messageTabControl.Height;
                messagePage.Width = this.messageTabControl.Width;*/

            /*RichTextBox textBox = new RichTextBox();
            textBox.Name = clientName + "richTextBox";
            textBox.WordWrap = true;
            textBox.Dock = DockStyle.Fill;*/
            if (IsClientMessage.Equals("Client"))
            {
                richTextBox1.SelectionColor = Color.Blue;
                richTextBox1.SelectionAlignment = HorizontalAlignment.Left;
            }
            else if (IsClientMessage.Equals("Server"))
            {
                richTextBox1.SelectionColor = Color.Green;
                richTextBox1.SelectionAlignment = HorizontalAlignment.Right;
            }
            richTextBox1.AppendText(message);
            richTextBox1.AppendText(System.Environment.NewLine) ;

            /*messagePage.Controls.Add(textBox);

                this.messageTabControl.TabPages.Add(messagePage);
            }
            else
            {
                Control.ControlCollection controls = messagePage.Controls;
                foreach (Control control in controls)
                {
                    if (control.Name.Equals(clientName + "richTextBox"))
                    {
                        RichTextBox textBox = control as RichTextBox;
                        if (textBox != null)
                        {
                            textBox.Text = textBox.Text + System.Environment.NewLine + message;
                            textBox.Invalidate();
                        }
                    }
                }
            }

            this.messageTabControl.Invalidate();*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (clientNameTextBox.Text == "" || ipAddressTextBox.Text == "" || portNumberTextbox.Text == "")
            {
                errorLabel.Text = "All fields are compulsory";
                return;                
            }
            else
            {
                if (connectButton.Text == "Connect")
                {
                    errorLabel.Text = String.Empty;
                    clientNameTextBox.ReadOnly = true;
                    ipAddressTextBox.ReadOnly = true;
                    portNumberTextbox.ReadOnly = true;
                    connectButton.Text = "Disconnect";
                }
                else
                {
                    clientNameTextBox.ReadOnly = false;
                    clientNameTextBox.Text = String.Empty;
                    ipAddressTextBox.Text = String.Empty;
                    ipAddressTextBox.ReadOnly = false;
                    portNumberTextbox.Text = String.Empty;
                    portNumberTextbox.ReadOnly = false;
                    connectButton.Text = "Connect";
                }
            }
        }
    }
}
