﻿//-----------------------------------------------------------------------
// <author> 
//     Rahul Dhawan
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="ICommunication.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain the callbackfn used by the communicator class.
// </summary>
//-----------------------------------------------------------------------

namespace Messenger
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents a Messager class to send and recieve message.
    /// </summary>
    public interface IMessager
    {
        /// <summary>
        /// Function triggered when the communication will successful or fail in message delivery where message is the message send and the ipaddress is the ip from it come and stauscode is the status of the code.
        /// </summary>
        void StatusCallback(/*string message, string ipaddress, StatusCode statuscode*/);

        /// <summary>
        /// Function triggered when the communication will recived message and they want to transfer.
        /// </summary>
        void DataCallback(/*string message, string ipaddres*/);
    }
}
