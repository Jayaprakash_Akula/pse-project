﻿//-----------------------------------------------------------------------
// <author>
//      Sooraj Tom
// </author>
// <reviewer>
//      Axel James
// </reviewer>
// <date>
//      11-Oct-2018
// </date>
// <summary>
//      Interface for the Image Processing module
// </summary>
// <copyright file="IImageProcessing.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace ImageProcessing
{
    using System;

    /// <summary>
    /// The IImageProcessing interface.
    /// This module gives functionality related to sharing screen, storing and retrieving screenshots.
    /// This module will be instantiated by the UI module.
    /// This module is dependent on Networking and Schema modules.
    /// </summary>
    public interface IImageProcessing
    {
        /// <summary>
        /// This method starts the screen sharing process.
        /// It is called by the server (professor) with the IP address of the client (student).
        /// </summary>
        /// <param name="clientIP">This parameter is the IP address of the target client computer</param>
        void GetSharedScreen(string clientIP);

        /// <summary>
        /// This method terminates the screen sharing session.
        /// </summary>
        void StopSharedScreen();

        /// <summary>
        /// This method saves the current screenshot into the permanent memory.
        /// </summary>
        void StoreScreenshot();

        /// <summary>
        /// This method retrieves images saved in the given time frame.
        /// </summary>
        /// <param name="startTime">The starting of the time frame</param>
        /// <param name="endTime">The ending of the time frame</param>
        void RetrieveScreenshot(DateTime startTime, DateTime endTime);
    }
}
