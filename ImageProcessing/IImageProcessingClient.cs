﻿//-----------------------------------------------------------------------
// <author> 
//     Ravindra Kumar
// </author>
//
// <date> 
//     12-10-2018 
// </date>
// 
// <reviewer> 
//     Sooraj Tom 
// </reviewer>
// 
// <copyright file="IImageProcessingClient.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This file contains interface for Client-side Image Processing. 
//      This interface is for my team members and not for the external teams.
// </summary>
//-----------------------------------------------------------------------

namespace ImageProcessing
{
    using System.Net;

    /// <summary>
    /// Prototype for client-side Image processing module.
    /// </summary>
    public interface IImageProcessingClient
    {
        /// <summary>
        /// Function which receive the signal to start the screen sharing.
        /// </summary>
        /// <param name="targetIP">IP address of Server</param>
        /// <param name="timeInterval">Time interval between two image packets.</param>
        /// <returns>true if success and false if some interrupt occur and failed to send.</returns>
        bool StartScreenSharing(IPAddress targetIP, uint timeInterval);

        /// <summary>
        /// Function which receive the signal to stop the screen sharing and disposes the timer.
        /// </summary>
        /// <returns>true if success and false if some interrupt occur and failed to stop the screen sharing.</returns>
        bool StopSharing();

        /// <summary>
        /// Function which receive the signal when some image packet is lost and a new image packet is requested.
        /// It will capture the screen and send it.( It don't register to timer).
        /// </summary>
        /// <param name="targetIP">IP address of Server</param>
        /// <returns>true if success and false if some interrupt occurred and failed to send.</returns>
        bool Resend(IPAddress targetIP);
    }
}
